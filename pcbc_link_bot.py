import logging
from telegram import Update
from telegram.ext import Updater, CommandHandler, PrefixHandler, MessageHandler, Filters, CallbackContext, \
    ConversationHandler, TypeHandler
from bitly_api import Connection
from link_processing import AffiliateLinkConverter
from config.config import Config

# Add config file

m_config = Config()
m_alc = AffiliateLinkConverter()
# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Add new user
USER, ID = range(2)


def addUser(update: Update, context: CallbackContext):
    if m_config.isAdmin(update.message.from_user['id']):
        update.message.reply_text("Please enter the user id.")
        return ID
    else:
        update.message.reply_text("You don't have the permission to create a new user.")
        return ConversationHandler.END


def getUserID(update: Update, context: CallbackContext):
    id = update.message.text
    context.user_data["user_id"] = id
    if id.isdigit():
        update.message.reply_text("Please enter the user name.")
        return USER
    else:
        update.message.reply_text("Your input was wrong. Please enter the user id again.")
        return ID


def getUserName(update: Update, context: CallbackContext):
    name = update.message.text
    if name.isalpha():
        m_config.addUser(context.user_data["user_id"], name)

        update.message.reply_text("User was successfully created!")
        return ConversationHandler.END
    else:
        update.message.reply_text("Your input was wrong. Please enter the user name again.")
        return USER


# Delete User
DELUSER = range(1)


def deleteUser(update: Update, context: CallbackContext):
    if m_config.isAdmin(update.message.from_user['id']):
        update.message.reply_text("Please enter the user id.")
        return DELUSER
    else:
        update.message.reply_text("You don't have the permission to remove a user.")
        return ConversationHandler.END


def deleteUserID(update: Update, context: CallbackContext):
    if m_config.isUser(int(update.message.text)):
        m_config.delUser(int(update.message.text))
        update.message.reply_text("The user was succesfully deleted!")
        return DELUSER
    else:
        update.message.reply_text("The user id is not know. Please enter the user id again.")
        return DELUSER


# Get list of current user name and id

def getUserList(update: Update, context: CallbackContext):
    if m_config.isAdmin(update.message.from_user['id']):
        userList = m_config.getUserList()
        replyText = "The current user list with user id:\n"
        for i in userList:
            replyText = replyText + "\n" + i
        update.message.reply_text(replyText)
    else:
        update.message.reply_text("You don't have the permission to create a new user.")


# Create Awin Link
GETAWIN, CREATEAWIN = range(2)
GETLINK, GETAWIN, CREATEAWIN = range(3)

def getAwinSecondClickRef(update: Update, context: CallbackContext):
    update.message.reply_text("Please enter a second click reference.")
    return GETLINK

def getAwinLink(update: Update, context: CallbackContext):
    if(update.message.text.isdigit()):
        context.user_data["clickref2"] = update.message.text
    elif(update.message.text != "#createAwin"):
        update.message.reply_text("The given click reference is invalid. Please enter a new click reference.")
    else:
        context.user_data["clickref2"] = ""
    update.message.reply_text("Please enter product link.")
    return GETAWIN


def getAwinLinkTitle(update: Update, context: CallbackContext):
    context.user_data["awinLink"] = update.message.text
    if m_alc.is_valid_link(context.user_data["awinLink"]):
        update.message.reply_text("Please enter link title.")
        return CREATEAWIN
    else:
        update.message.reply_text("The given link is invalid. Please enter the link again.")
        return GETAWIN


def createAwinLink(update: Update, context: CallbackContext):
    linkTitle = update.message.text
    partnerName = m_alc.getPartnerName(context.user_data["awinLink"])
    awinLink = m_alc.getAwinLink(context.user_data["awinLink"], m_config.getAwinIDs(), partnerName,
                                 update.message.from_user['id'], context.user_data["clickref2"])
    shortUrl = createShortLink(awinLink, linkTitle, partnerName)
    message = "[{}] {}\n{}".format(partnerName, linkTitle, shortUrl)
    update.message.reply_text(message)
    return ConversationHandler.END

# Create Media-Saturn link
GETMS, CREATEMS = range(2)
def getMediaSaturnLink(update: Update, context: CallbackContext):
    update.message.reply_text("Please enter product link.")
    return GETMS

def getMediaSaturnLinkTitle(update: Update, context: CallbackContext):
    context.user_data["mediaSaturnLink"] = update.message.text
    if m_alc.is_valid_link(context.user_data["mediaSaturnLink"]):
        update.message.reply_text("Please enter link title.")
        return CREATEMS
    else:
        update.message.reply_text("The given link is invalid. Please enter the link again.")
        return GETMS

def createMediaSaturnLink(update: Update, context: CallbackContext):
    linkTitle = update.message.text
    mediaSaturnLink = m_alc.getMediaSaturnLink(context.user_data["mediaSaturnLink"], m_config.getMediaSaturnIDs(), update.message.from_user['id'])
    partnerName = m_alc.getPartnerName(context.user_data["mediaSaturnLink"])
    shortUrl = createShortLink(mediaSaturnLink, linkTitle, partnerName)
    message = "[{}] {}\n{}".format(partnerName, linkTitle, shortUrl)
    update.message.reply_text(message)
    return ConversationHandler.END

# Create Amazon link
GETAZ,CHOOSEAZ, CREATEAZ = range(3)
def getAmazonLink(update: Update, context: CallbackContext):
    update.message.reply_text("Please enter product link.")
    return GETAZ

def getAmazonLinkTitle(update: Update, context: CallbackContext):
    context.user_data["amazonLink"] = update.message.text
    if m_alc.is_valid_link(context.user_data["amazonLink"]):
        update.message.reply_text("Please enter link title.")
        return CHOOSEAZ
    else:
        print(context.user_data["amazonLink"])
        update.message.reply_text("The given link is invalid. Please enter the link again.")
        return GETAZ

def getAmazonClickRef(update: Update, context: CallbackContext):
    context.user_data["amazonLinkTitle"] = update.message.text
    update.message.reply_text("Please choose your click reference: \n[1] PCBC\n[2] Ghostbuilder")
    return CREATEAZ


def createAmazonLink(update: Update, context: CallbackContext):
    clickRef = update.message.text
    if clickRef.isdigit():
        if int(clickRef) == 1:
            clickRef = "pcbuildersclub-21"
        elif int(clickRef) == 2:
            clickRef = "ghostbuilder-21"
        else:
            update.message.reply_text("The given number is wrong. Please enter a 1 for PCBD or a 2 for Ghostbuilder.")
            return CREATEAZ
    else:
        update.message.reply_text("The given number is wrong. Please enter a 1 for PCBD or a 2 for Ghostbuilder.")
        return CREATEAZ
    amazonLink = m_alc.getAmazonLink(context.user_data["amazonLink"], clickRef)
    partnerName = m_alc.getPartnerName(context.user_data["amazonLink"])
    shortUrl = createShortLink(amazonLink, context.user_data["amazonLinkTitle"], partnerName)
    message = "[{}] {}\n{}".format(partnerName, context.user_data["amazonLinkTitle"], shortUrl)
    update.message.reply_text(message)
    return ConversationHandler.END

# Shorten Link

def createShortLink(affiliateLink, linkTitle, partnerName):
    try:
        title = "[{}] {}".format(partnerName, linkTitle)
        c = Connection(access_token=m_config.getBitlyAPIToken())
        shortUrl = c.shorten(affiliateLink, preferred_domain='bit.ly')["url"]
        c.user_link_edit(shortUrl, edit="title", title=title)
        return shortUrl
    except IndexError:
        print("invalid link")
        return "error: no valid link"


# Callback function when user cancels a conversation

def cancel(update: Update, context: CallbackContext):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)

    return ConversationHandler.END


# Standard bot commands

def start(update: Update, context: CallbackContext):
    user_id = m_config.getUserID()
    if update.message.from_user['id'] in user_id:
        update.message.reply_text("Welcome to the PCBC Link Bot.")
    else:
        update.message.reply_text("Pardon, but your ID does not match with any saved User ID.")


def help(update):
    update.message.reply_text('Please write an email to lukaszach@protonmail.com. Thank you.')


def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(m_config.getBotAPIToken(), use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # conversation handler to add a new user
    chAddUser = ConversationHandler(
        entry_points=[PrefixHandler('#', 'addUser', addUser)],

        states={
            ID: [MessageHandler(Filters.text, getUserID)],
            USER: [MessageHandler(Filters.text, getUserName)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # conversation handler to delete user
    chDeleteUser = ConversationHandler(
        entry_points=[PrefixHandler('#', 'deleteUser', deleteUser)],

        states={
            DELUSER: [MessageHandler(Filters.text, deleteUserID)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # conversation handler to create a awin-link with one click reference
    chAwinLink = ConversationHandler(
        entry_points=[PrefixHandler('#', 'createAwin', getAwinLink)],

        states={
            GETAWIN: [MessageHandler(Filters.text, getAwinLinkTitle)],
            CREATEAWIN: [MessageHandler(Filters.text, createAwinLink)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # conversation handler to create a awin-link with a second click reference
    chAwinLinkClickref2 = ConversationHandler(
        entry_points=[PrefixHandler('#', 'createAwin2', getAwinSecondClickRef)],

        states={
            GETLINK: [MessageHandler(Filters.text, getAwinLink)],
            GETAWIN: [MessageHandler(Filters.text, getAwinLinkTitle)],
            CREATEAWIN: [MessageHandler(Filters.text, createAwinLink)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # conversation handler to create a Media-Saturn-link
    chMediaSaturn = ConversationHandler(
        entry_points=[PrefixHandler('#', 'createMediaSaturn', getMediaSaturnLink)],

        states={
            GETMS: [MessageHandler(Filters.text, getMediaSaturnLinkTitle)],
            CREATEMS: [MessageHandler(Filters.text, createMediaSaturnLink)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # conversation handler to create a Amazon-link
    chAmazon = ConversationHandler(
        entry_points=[PrefixHandler('#', 'createAmazon', getAmazonLink)],

        states={
            GETAZ: [MessageHandler(Filters.text, getAmazonLinkTitle)],
            CHOOSEAZ: [MessageHandler(Filters.text, getAmazonClickRef)],
            CREATEAZ: [MessageHandler(Filters.text, createAmazonLink)]
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(chAddUser)
    dp.add_handler(chDeleteUser)
    dp.add_handler(PrefixHandler('#', "getUserList", getUserList))
    dp.add_handler(chAwinLink)
    dp.add_handler(chAwinLinkClickref2)
    dp.add_handler(chMediaSaturn)
    dp.add_handler(chAmazon)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
