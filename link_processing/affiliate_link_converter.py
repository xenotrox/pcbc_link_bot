import requests


class AffiliateLinkConverter:
    def __init__(self):
        pass

    def is_valid_link(self, link):
        request = requests.get(link)
        if request.status_code < 400 or request.status_code == 503:
            return True
        else:
            return False

    def getAwinLink(self, link, awinIDs, partnerName, clickref, clickref2):
        link = link.replace(":", "%3A")
        link = link.replace("/", "%2F")
        link = link.replace("+", "%2B")

        mid = awinIDs[partnerName.lower()]
        affid = awinIDs["pcbc"]

        return "https://www.awin1.com/cread.php?awinmid={}&awinaffid={}&clickref={}&clickref2={}&ued={}".format(mid,
                                                                                                                affid,
                                                                                                                clickref,
                                                                                                                clickref2,
                                                                                                                link)

    def getPartnerName(self, link):
        return link.split(".")[1].title()

    def getProductID(self, link):
        return link.split(".")[-2].split("-")[-1]

    def getASIN(self, link):
        return link.split("dp/")[-1].split("?")[0]

    def getMediaSaturnLink(self, link, mediaSaturnIDs, clickref):
        partnerName = self.getPartnerName(link)
        productID = self.getProductID(link)
        partnerID = mediaSaturnIDs[partnerName.lower()]
        partnerFID = mediaSaturnIDs[partnerName.lower() + "fid"]
        return "https://pvn.{}.de/trck/eclick/{}?prodid={}&fid={}&subid={}".format(partnerName,
                                                                                   partnerID,
                                                                                   productID,
                                                                                   partnerFID,
                                                                                   clickref)

    def getAmazonLink(self, link, clickref):
        asin = self.getASIN(link)
        return "https://www.amazon.de/dp/{}?tag={}".format(asin, clickref)