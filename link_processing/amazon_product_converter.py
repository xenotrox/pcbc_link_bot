import urllib.request


class AmazonProductConverter:
    def __init__(self, product_name, asin):
        """
        Constructor of the AmazonProductConverter class.
        :var _amazon_tracking_ids: List of the tracking IDs of the different countries.
        :var _amazon_link_parts: List of the parts that are needed to generate a amazon affiliate link.
        :param product_name: Product name
        :param asin: Amazon Standard Identification Number; The ASIN can differentiate between countries or is the same.
        """
        self._product_name = product_name
        self._asin = asin

        self.affiliate_link = ""
        self._amazon_tracking_ids = ["twit005-20",  # Tracking ID USA
                                     "twit00-21",  # Tracking ID UK
                                     "pcbuildersclub-21",  # Tracking ID Germany
                                     "twit00a-20",  # Tracking ID Canada
                                     "twit05-22"]  # Tracking ID Australia
        self._amazon_link_parts = ["http://a-fwd.com/com=",
                                   "&uk=",
                                   "&de=",
                                   "&ca=",
                                   "&au=",
                                   "&asin-com=",
                                   "&asin-uk=",
                                   "&asin-de=",
                                   "&asin-ca=",
                                   "&asin-au="]

    def _convert_multi_asin_link(self):
        """
        Generates a affiliate link with multiple ASINs.
        :return: link
        :rtype: str
        """
        amazon_link = ""
        for i in range(len(self._amazon_link_parts)):
            if i < len(self._amazon_tracking_ids):
                amazon_link += self._amazon_link_parts[i] + self._amazon_tracking_ids[i]
            else:
                amazon_link += self._amazon_link_parts[i] + self._asin[i - len(self._amazon_tracking_ids)]
        self.affiliate_link = amazon_link

    def _convert_single_asin_link(self):
        """
        Generates a affiliate link with one ASIN.
        :return:  link
        :rtype: str
        """
        amazon_link = ""
        for i in range(len(self._amazon_link_parts)):
            if i < len(self._amazon_tracking_ids):
                amazon_link += self._amazon_link_parts[i] + self._amazon_tracking_ids[i]
            else:
                amazon_link += self._amazon_link_parts[i] + self._asin
        self.affiliate_link = amazon_link

    def get_affiliate_link(self):
        """
        Checks if the product is has multiple ASIN and than creates a single or multi ASIN affiliate link.
        :return: link
        :rtype: str
        """
        if isinstance(self._asin, str):
            self._convert_single_asin_link()
        elif len(self._asin) > 1:
            self._convert_multi_asin_link()

    def is_valid_link(self):
        """
        Checks that a given URL is reachable.
        :rtype: bool
        """
        link = self.affiliate_link
        request = urllib.request.Request(link)
        request.get_method = lambda: 'HEAD'

        try:
            urllib.request.urlopen(request)
            return True
        except urllib.request.HTTPError:
            return False

